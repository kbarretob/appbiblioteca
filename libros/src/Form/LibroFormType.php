<?php

namespace App\Form;

use App\Entity\Libros;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LibroFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->add('titulo', Texttype::class, ['attr' => ['class' => 'form-control']]);
        $builder->add('descripcion', TextType::class, ['attr' => ['class' => 'form-control']]);
        $builder->add('anio_publicidad', NumberType::class, ['attr' => ['class' => 'form-control']]);
        $builder->add('autor', Texttype::class, ['attr' => ['class' => 'form-control']]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Libros::class,
        ]);
    }
}
