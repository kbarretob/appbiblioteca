<?php

namespace App\Controller;

use App\Entity\Libros;
use App\Repository\LibrosRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ListController extends AbstractController
{
    /**
     * @Route("/list", name="app_list")
     */
    public function index(LibrosRepository $librosRepository): Response
    {
        return $this->render('list/index.html.twig', [
            'libros' => $librosRepository->findAll(),
        ]);
    }
    
    /**
     * @Route("/delete/{id}", name="app_delete")
     */
    public function delete(Libros $libros, ManagerRegistry $doctrine): RedirectResponse
    {
        $em = $doctrine->getManager();
        $em->remove($libros);
        $em->flush();
        return $this->redirectToRoute('app_list');
    }
    

}
