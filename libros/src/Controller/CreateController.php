<?php

namespace App\Controller;
use App\Entity\Libros;
use App\Form\LibroFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Mailer\Event\Message;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;


class CreateController extends AbstractController
{
    /**
     * @Route("/create", name="app_create")
     */
    public function index(Request $request, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(LibroFormType::class, new libros());
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $libro = $form->getData();
            $em = $doctrine->getManager();
            $em->persist($libro);
            $em->flush();
            
            return $this->redirectToRoute('app_list');
        }


        return $this->render('create/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
