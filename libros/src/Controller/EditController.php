<?php

namespace App\Controller;
use App\Entity\Libros;
use App\Form\LibroFormType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;

class EditController extends AbstractController
{
    /**
     * @Route("/edit/{id}", name="app_edit")
     */
    public function index(Libros $libros, Request $request, ManagerRegistry $doctrine): Response
    {
        $form = $this->createForm(LibroFormType::class, $libros);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()){
            $libro = $form->getData();
            $em = $doctrine->getManager();
            $em->persist($libro);
            $em->flush();

            return $this->redirectToRoute('app_list');
        
        };
        return $this->render('create/index.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
