<?php

namespace App\DataFixtures;

use App\Entity\Libros;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        for ($i =0 ; $i < 6; $i++){
            $libro = new Libros();
            $libro->setTitulo("Cumbres Borrascosas ".strval($i));
            $libro->setDescripcion("Publicado por Editorial Alma".strval($i));
            $libro->setAnioPublicidad(random_int(1800,1900));
            $libro->setAutor("El autor del libro es: ".strval($i+1));
            $manager->persist($libro);
            $manager->flush();      }

        $manager->flush();
    }
}
